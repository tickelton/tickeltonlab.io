<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../../main.css">
    <title>A Very Short Introduction to NTP Timestamps</title>
</head>
<body>

<div class="header-right"><a href="https://tickelton.gitlab.io">HOME</a></div>

<h2>A Very Short Introduction to NTP Timestamps</h2>

<!--
<p><img src="time_formats.png" alt="Visual represntation of different timestamp formats" title="Comparison of timestamp formats" /></p>
-->

<p>
Over the past couple of weeks I have been working on several pieces of
software that critically rely on precise time synchronization across
different components.
<br>
I found it quite surprising to come across multiple third party
components that caused problems because they implemented their own
NTP clients and got the processing of NTP timestamps wrong.
<br>
This is what prompted me to write this very short introduction
to NTP timestamps and how to convert them to other common
timestamp formats.
</p>

<h2>TIMEVAL, TIMESPEC, and their likes</h2>

<p>
The most common timestamp format one encounters in systems programming
is arguable <em>struct timeval</em> which is defined on Windows
in <strong>winsock.h</strong> and on Unix-like systems in
<strong>sys/time.h</strong>. This is what we get by e.g. calling
<strong>gettimeofday()</strong>.
<br>
<em>struct timeval </em> is typically defined like this:
<pre><code>  struct timeval {
      time_t      tv_sec;     /* seconds */
      suseconds_t tv_usec;    /* microseconds */
  };
</code></pre>
Which on a 32 bit system translates to the following layout
in memory:
<br>
<pre><code>   0                   1                   2                   3
   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |                             tv_sec                            |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |                            tv_usec                            |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
</code></pre>
</p>

<p>
As we can see here the timestamp consists of two values. One
representing seconds, the other representing microseconds.
<br>
The sizes of the variables representing those two fields will vary
across different systems and architectures. E.g. on 64 bit systems
both will usually be 64 bits wide instead of the 32 bits depicted above.
<br>
Additionally the meaning of the seconds component will vary depending
on context. When reading the current time on a Unix system <em>tv_sec</em>
will represent the seconds since January 1st 1970, whereas the
reference point can be a different one on other systems.
<br>
The same structure is also widely used across APIs to pass relative
time information to arbitrary functions, for example timeouts for
requests. In that case <em>tv_sec</em> will just represent an absolute number
of seconds.
</p>

<p>
Since we are concerned with synchronizing times across applications
and focusing on Unix systems, from here on <em>tv_sec</em> is assumed to 
represent seconds since January 1st 1970.
<p>

<p>
<em>tv_usec</em> on the other hand will always contain a number between
zero and 999,999 representing the microseconds part of the given
timestamp and is therefore not directly dependent on the context
in which the timestamp is used.<br>
</p>

<p>
There are also several other similar formats in common use which
all behave mostly the same as described above but only differ
in the resolution of the sub-second component.
<br>
For example <em>struct timespec</em> is typically defined
as:
<pre><code>  struct timespec {
      time_t  ts_sec;    /* seconds */
      long   ts_nsec;    /* nanoseconds */
  };
</code></pre>
Which means that the only difference to <em>struct timeval</em> is that the
sub-second component represents nanoseconds instead of microseconds.
</p>

<h2>NTP timestamp format</h2>

<p>
In contrast to the structures described so far we now come to
NTP timestamps which are defined in <a href="https://datatracker.ietf.org/doc/html/rfc1305">RFC 1305</a> as
follows:
<pre><code>   0                   1                   2                   3
   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |                   Integer part of seconds                     |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |                 Fractional part of seconds                    |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
</code></pre>
In the upcoming example code we will use the following structure to represent
them:
<pre><code>  struct ntp_ts_t {
      uint32_t seconds;
      uint32_t fraction;
  };
</code></pre>
</p>

<p>
The first difference to the other types of timestamps we have covered
so far is that the fields of an NTP timestamp are always 32 bits wide,
regardless of the underlying architecture.
<br>
(There is also another NTP timestamp format that is 128 bits wide,
usually called NTP datestamp, but we will not cover that here.)
</p>
<p>
The second difference, to Unix timestamps in particular, is that the
seconds part of the NTP timestamp refers to the seconds since January
1st 1900, not 1970.
</p>
<p>
From what we have learned so far the conversion between Unix and
NTP timestamps should be pretty straight forward: <br>
Just add or subtract 70 years to or from the seconds part and watch
out for potential overflows on systems that don't use 32 bit integers.
</p>
<p>
Unfortunately there still is the fractional part of the NTP timestamp
and this is where it gets a little tricky.
</p>
<p>
It seems easy to conclude that <em>"Fractional part of seconds"</em> here
effectively means the same as microseconds in the above timestamps.
<br>
After all 100 microseconds are .1 seconds or 1/10 of a second which
kind of is a fractional part of a second.<br>
And as far as I can tell this seems to have been the reasoning of
the authors of the software that gave me trouble over the past
couple of days.
</p>
<p>
What <em>"Fractional part of seconds"</em> actually means here though is this:
<br>
A value of <strong>1</strong> represents
<strong>1/(2^32)</strong> of a second which is about .2 nanoseconds.<br>
</p>

<h2>How to convert between them</h2>

<p>
So how do we convert between Unix and NTP timestamps ?
</p>
<p>
As already mentioned converting the seconds part is trivial.
Depending on the direction of the conversion, we just add
or remove 70 years, which is 2208988800 seconds.
</p>
<p>
To convert the fractional/sub-second part here's what we do:
<br>
To convert from NTP to Unix time we multiply by an appropriate
factor to get to the required subdivision of seconds, e.g.
10^6 in the case of microseconds, and divide the result by 2^32.
<br>
The conversion from Unix to NTP time is just the opposite. We
multiply by 2^32 and divide the result by 10^6.
<br>
Since the intermediate values can get larger than 32 bits
we'll have to watch out for overflows and use sufficiently large
types for the conversion.
</p>

<p>
Now that we know what to do, here's some example C code in
that performs the actual conversion:
<pre><code>  void ntp_to_timeval(struct ntp_ts_t *ntp, struct timeval *tv) {
    tv->tv_sec = ntp->seconds - 2208988800;
    tv->tv_usec = (uint32_t)((double)ntp->fraction * 1.0e6 / (double)(1LL << 32));
  }
 
  void timeval_to_ntp(struct timeval *tv, struct ntp_ts_t *ntp) {
    ntp->seconds = tv->tv_sec + 2208988800;
    ntp->fraction =
        (uint32_t)((double)(tv->tv_usec + 1) * (double)(1LL << 32) * 1.0e-6);
  }
</code></pre>
</p>


<footer>
last update: June 27, 2021
</footer>

</body>
</html>
